package app

import (
	"flamingo.me/flamingo/core/gotemplate"
	"flamingo.me/flamingo/framework/dingo"
	"flamingo.me/flamingo/framework/router"
	"flamingo.me/flamingo/framework/template"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app/domain"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app/infrastructure"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app/interfaces"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app/interfaces/controller"
)

type (
	// Module basic struct
	Module struct{}

	routes struct {
		home   *controller.HomeController
		rating *controller.RatingController
	}
)

// Inject dependencies
func (r *routes) Inject(
	homeController *controller.HomeController,
	ratingController *controller.RatingController,
) {
	r.home = homeController
	r.rating = ratingController
}

// Depends on other modules
func (m *Module) Depends() []dingo.Module {
	return []dingo.Module{
		new(gotemplate.Module),
	}
}

// Configure Rating module
func (m *Module) Configure(injector *dingo.Injector) {
	template.BindFunc(injector, "random", new(interfaces.RandomIntFunc))
	template.BindFunc(injector, "for", new(interfaces.ForFunc))
	template.BindFunc(injector, "barType", new(interfaces.BarTypeFunc))

	injector.Bind((*domain.RatingRepository)(nil)).To(new(infrastructure.RatingRepository))
	injector.Bind((*domain.ProductRepository)(nil)).To(new(infrastructure.ProductRepository))

	router.Bind(injector, new(routes))
}

// Routes served by this module
func (r *routes) Routes(registry *router.Registry) {
	registry.Route("/", "home")
	registry.HandleGet("home", r.home.Home)

	registry.Route("/products", "products")
	registry.HandleGet("products", r.home.ProductList)

	registry.Route("/rating/$pid<[0-9]+>", "rating")
	registry.HandleGet("rating", r.rating.View)

	registry.Route("/rating/new", "rating.product.form")
	registry.HandleGet("rating.product.form", r.rating.ProductForm)

	registry.Route("/rating/$pid<[0-9]+>/new", "rating.new")
	registry.HandleGet("rating.new", r.rating.Form)

	registry.Route("/rating/", "rating.post")
	registry.HandlePost("rating.post", r.rating.FormPost)

	registry.Route("/rating/success", "rating.success")
	registry.HandleGet("rating.success", r.rating.Success)
}
