package form_test

import (
	"context"
	"net/url"
	"testing"

	"flamingo.me/flamingo/core/form/domain"
	"flamingo.me/flamingo/framework/web"
	"github.com/go-test/deep"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app/interfaces/controller/form"
)

func TestRatingFormService_ParseFormData(t *testing.T) {
	type args struct {
		formValues url.Values
	}
	tests := []struct {
		name string
		args args
		want form.RatingFormData
	}{
		{
			name: "valid case",
			args: args{
				formValues: url.Values{
					"product_id": []string{"7"},
					"name":       []string{"Chris"},
					"stars":      []string{"5"},
					"title":      []string{"A title"},
					"text":       []string{"The text"},
				},
			},
			want: form.RatingFormData{
				ProductID: "7",
				Name:      "Chris",
				Stars:     "5",
				Title:     "A title",
				Text:      "The text",
			},
		},
		{
			name: "work for conform",
			args: args{
				formValues: url.Values{
					"product_id": []string{" tra7tra "},
					"name":       []string{"•?((¯°·._.•Chris•._.·°¯))؟•"},
					"stars":      []string{"  give 5 stars please!! ★★★★★"},
					"title":      []string{"   A title which must be trimmed  !    "},
					"text":       []string{"   The text  to be trimmed   "},
				},
			},
			want: form.RatingFormData{
				ProductID: "7",
				Name:      "Chris",
				Stars:     "5",
				Title:     "A title which must be trimmed  !",
				Text:      "The text  to be trimmed",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			formService := &form.RatingFormService{}
			got, _ := formService.ParseFormData(context.Background(), &web.Request{}, tt.args.formValues)
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestRatingFormService_ValidateFormData(t *testing.T) {
	type args struct {
		data interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    domain.ValidationInfo
		wantErr bool
	}{
		{
			name: "valid",
			args: args{
				data: form.RatingFormData{
					ProductID: "1",
					Name:      "Test",
					Stars:     "17",
					Title:     "A title",
					Text:      "The text",
				},
			},
			want: domain.ValidationInfo{
				FieldErrors:   map[string][]domain.Error{},
				GeneralErrors: nil,
				IsValid:       true,
			},
			wantErr: false,
		},
		{
			name: "invalid required fields",
			args: args{
				data: form.RatingFormData{
					ProductID: "",
					Name:      "",
					Stars:     "",
					Title:     "",
					Text:      "",
				},
			},
			want: domain.ValidationInfo{
				FieldErrors: map[string][]domain.Error{
					"productID": {
						{
							Tag:          "required",
							MessageKey:   "formerror_productID_required",
							DefaultLabel: "ProductID wrong",
						},
					},
					"name": {
						{
							Tag:          "required",
							MessageKey:   "formerror_name_required",
							DefaultLabel: "Name wrong",
						},
					},
					"stars": {
						{
							Tag:          "required",
							MessageKey:   "formerror_stars_required",
							DefaultLabel: "Stars wrong",
						},
					},
					"title": {
						{
							Tag:          "required",
							MessageKey:   "formerror_title_required",
							DefaultLabel: "Title wrong",
						},
					},
					"text": {
						{
							Tag:          "required",
							MessageKey:   "formerror_text_required",
							DefaultLabel: "Text wrong",
						},
					},
				},
				GeneralErrors: nil,
				IsValid:       false,
			},
			wantErr: false,
		},
		{
			name: "invalid input",
			args: args{
				data: map[string]string{},
			},
			want: domain.ValidationInfo{
				FieldErrors:   nil,
				GeneralErrors: nil,
				IsValid:       false,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			formService := &form.RatingFormService{}
			got, err := formService.ValidateFormData(tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("RatingFormService.ValidateFormData() error = %v, wantErr %v", err, tt.wantErr)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}
