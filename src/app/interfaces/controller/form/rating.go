package form

import (
	"context"
	"errors"
	"net/url"

	"flamingo.me/flamingo/core/form/application"
	"flamingo.me/flamingo/core/form/domain"
	"flamingo.me/flamingo/framework/web"
	formlib "github.com/go-playground/form"
	"github.com/leebenson/conform"
	"gopkg.in/go-playground/validator.v9"
)

type (
	// RatingFormData represents the data of the rating form
	RatingFormData struct {
		ProductID string `form:"product_id" validate:"required" conform:"num"`
		Name      string `form:"name" validate:"required" conform:"name"`
		Stars     string `form:"stars" validate:"required" conform:"num"`
		Title     string `form:"title" validate:"required" conform:"trim"`
		Text      string `form:"text" validate:"required" conform:"trim"`
	}

	// RatingFormField represents a Field of the rating form
	RatingFormField struct {
		Type        string
		ID          string
		Name        string
		Label       string
		Value       string
		Icon        string
		HasFeedback bool
		HasError    bool
		Errors      []string
	}

	// RatingFormService is the flamingo form service implementation for the rating form
	RatingFormService struct{}
)

var (
	// use a single instance of Decoder, it caches struct info
	decoder *formlib.Decoder

	// RatingFormStructure defines the rating form
	RatingFormStructure = []RatingFormField{
		{
			Type:     "input",
			ID:       "name",
			Name:     "name",
			Label:    "User name",
			Value:    "",
			Icon:     "user",
			HasError: false,
			Errors:   nil,
		},
		{
			Type:     "stars",
			ID:       "stars",
			Name:     "stars",
			Label:    "Stars",
			Value:    "",
			Icon:     "",
			HasError: false,
			Errors:   nil,
		},
		{
			Type:     "input",
			ID:       "title",
			Name:     "title",
			Label:    "Title",
			Value:    "",
			Icon:     "header",
			HasError: false,
			Errors:   nil,
		},
		{
			Type:     "textarea",
			ID:       "text",
			Name:     "text",
			Label:    "Text",
			Value:    "",
			Icon:     "edit",
			HasError: false,
			Errors:   nil,
		},
	}
)

// ParseFormData - from FormService interface
func (form *RatingFormService) ParseFormData(_ context.Context, _ *web.Request, formValues url.Values) (interface{}, error) {
	if decoder == nil {
		decoder = formlib.NewDecoder()
	}

	var formData RatingFormData
	// formData is a valid struct, so no errors can happen here
	_ = decoder.Decode(&formData, formValues)
	_ = conform.Strings(&formData)

	return formData, nil
}

// ValidateFormData - from FormService interface
func (form *RatingFormService) ValidateFormData(data interface{}) (domain.ValidationInfo, error) {
	if formData, ok := data.(RatingFormData); ok {
		validate := validator.New()

		return application.ValidationErrorsToValidationInfo(validate.Struct(formData)), nil
	}

	return domain.ValidationInfo{}, errors.New("cannot convert to RatingFormData")
}
