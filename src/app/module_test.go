package app_test

import (
	"testing"

	"flamingo.me/dingo"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app"
)

func TestModule_Configure(t *testing.T) {
	if err := dingo.TryModule(new(app.Module)); err != nil {
		t.Error(err)
	}
}
