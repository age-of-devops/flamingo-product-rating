package metrics

import (
	"context"
	"strconv"
	"time"

	"flamingo.me/flamingo/framework/event"
	"flamingo.me/flamingo/framework/flamingo"
	"flamingo.me/flamingo/framework/opencensus"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app/domain"
	"go.opencensus.io/stats"
	"go.opencensus.io/tag"
)

type (
	// StartUpMetrics subscribes to the AppStartup
	StartUpMetrics struct {
		ratingRepository domain.RatingRepository
		logger           flamingo.Logger
	}
)

// Inject dependencies
func (s *StartUpMetrics) Inject(r domain.RatingRepository, l flamingo.Logger) {
	s.ratingRepository = r
	s.logger = l
}

// Notify starts the rating metrics on the AppStartupEvent
func (s *StartUpMetrics) Notify(event event.Event) {
	switch event.(type) {
	case *flamingo.AppStartupEvent:
		s.logger.Info("Start rating metrics")
		s.ratingsMetrics()
	}
}

func (s *StartUpMetrics) ratingsMetrics() {
	ticker = time.NewTicker(5 * time.Second)
	go func() {
		for range ticker.C {
			s.logger.Debug("collect rating metrics from DB")
			amount, errA := s.ratingRepository.Count()
			amountsByProduct, errB := s.ratingRepository.Amounts()
			if errA != nil || errB != nil {
				continue
			}

			ctx, _ := tag.New(context.Background(), tag.Upsert(keyProductID, "ALL"), tag.Upsert(opencensus.KeyArea, "-"))
			stats.Record(ctx, stat.M(amount))

			if amountsByProduct == nil {
				continue
			}

			for pid, amount := range *amountsByProduct {
				ctx, _ := tag.New(context.Background(), tag.Upsert(keyProductID, strconv.Itoa(pid)), tag.Upsert(opencensus.KeyArea, "-"))
				stats.Record(ctx, stat.M(int64(amount)))
			}
		}
	}()
}
