package metrics

import (
	"time"

	"flamingo.me/dingo"
	"flamingo.me/flamingo/framework/event"
	"flamingo.me/flamingo/framework/opencensus"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/db"
	"go.opencensus.io/stats"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/tag"
)

var (
	stat            = stats.Int64("rating/metrics/amount", "Amount of ratings", stats.UnitDimensionless)
	keyProductID, _ = tag.NewKey("productID")
	ticker          *time.Ticker
)

type (
	// Module basic struct
	Module struct{}
)

func init() {
	opencensus.View("rating/metrics/amount", stat, view.LastValue(), keyProductID)
}

// Configure Metrics module
func (m *Module) Configure(injector *dingo.Injector) {
	injector.BindMulti((*event.Subscriber)(nil)).To(&ShutdownMetrics{})
	injector.BindMulti((*event.Subscriber)(nil)).To(&StartUpMetrics{})
}

// Depends on other modules
func (m *Module) Depends() []dingo.Module {
	return []dingo.Module{
		new(app.Module),
		new(opencensus.Module),
		new(db.Module),
	}
}
