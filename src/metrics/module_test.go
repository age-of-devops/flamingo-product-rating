package metrics_test

import (
	"testing"

	"flamingo.me/dingo"
	"flamingo.me/flamingo/framework/config"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/metrics"
)

func TestModule_Configure(t *testing.T) {
	cfgModule := &config.Module{
		Map: config.Map{
			"opencensus.serviceAddr":     "",
			"opencensus.serviceName":     "",
			"opencensus.jaeger.enable":   false,
			"opencensus.jaeger.endpoint": "",
		},
	}

	if err := dingo.TryModule(cfgModule, new(metrics.Module)); err != nil {
		t.Error(err)
	}
}
