package metrics

import (
	"flamingo.me/flamingo/framework/event"
	"flamingo.me/flamingo/framework/flamingo"
)

type (
	// ShutdownMetrics handles the graceful app shutdown for the db connection
	ShutdownMetrics struct{}
)

// Notify handles the incoming event if it is an AppShutdownEvent and closes the db connection
func (s *ShutdownMetrics) Notify(event event.Event) {
	switch event.(type) {
	case *flamingo.AppShutdownEvent:
		ticker.Stop()
	}
}
