package application

import (
	"flamingo.me/flamingo/framework/event"
	"flamingo.me/flamingo/framework/flamingo"
)

type (
	// StartUpMigrations subscribes to the AppStartup
	StartUpMigrations struct {
		migrator *Migrator
		logger   flamingo.Logger
	}
)

// Inject dependencies
func (s *StartUpMigrations) Inject(
	m *Migrator,
	l flamingo.Logger,
) {
	s.migrator = m
	s.logger = l
}

// Notify handles the automigration if configured on the AppStartupEvent
func (s *StartUpMigrations) Notify(event event.Event) {
	switch event.(type) {
	case *flamingo.AppStartupEvent:
		s.logger.Info("Run auto migrations...")
		err := s.migrator.Up(nil)
		if err != nil {
			panic(err)
		}
	}
}
