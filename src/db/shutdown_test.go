package db_test

import (
	"testing"

	"flamingo.me/flamingo/framework/event"
	"flamingo.me/flamingo/framework/flamingo"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/db"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/db/mocks"
)

func TestShutdownSubscriber_Notify(t *testing.T) {
	type args struct {
		event event.Event
	}
	tests := []struct {
		name      string
		args      args
		wantClose bool
	}{
		{
			name: "Close on shutdown",
			args: args{
				event: &flamingo.AppShutdownEvent{},
			},
			wantClose: true,
		},
		{
			name: "No close on other event",
			args: args{
				event: nil,
			},
			wantClose: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			mockDB, mock, err := sqlmock.New()
			if err != nil {
				t.Fatal(err)
			}
			defer mockDB.Close()
			sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

			if tt.wantClose {
				mock.ExpectClose()
			}

			dbMock := &mocks.DB{}
			dbMock.On("Connection").Return(sqlxDB)

			s := &db.ShutdownSubscriber{}
			s.Inject(dbMock)

			s.Notify(tt.args.event)

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Error(err)
			}
		})
	}
}
