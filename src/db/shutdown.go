package db

import (
	"flamingo.me/flamingo/framework/event"
	"flamingo.me/flamingo/framework/flamingo"
)

type (
	// ShutdownSubscriber handles the graceful app shutdown for the db connection
	ShutdownSubscriber struct {
		db DB
	}
)

// Inject dependencies
func (s *ShutdownSubscriber) Inject(db DB) {
	s.db = db
}

// Notify handles the incoming event if it is an AppShutdownEvent and closes the db connection
func (s *ShutdownSubscriber) Notify(event event.Event) {
	switch event.(type) {
	case *flamingo.AppShutdownEvent:
		// no error handling here because it's app shutdown. the connection will be hard aborted anyways
		_ = s.db.Connection().Close()
	}
}
