package db_test

import (
	"testing"

	"flamingo.me/dingo"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/db"
)

func TestModule_Configure(t *testing.T) {
	if err := dingo.TryModule(new(db.Module)); err != nil {
		t.Error(err)
	}
}
