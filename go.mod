module gitlab.com/age-of-devops/flamingo-product-rating

require (
	flamingo.me/dingo v0.1.3
	flamingo.me/flamingo v0.0.0-20190121221152-a5db848cb902
	git.apache.org/thrift.git v0.12.0 // indirect
	github.com/DATA-DOG/go-sqlmock v1.3.0
	github.com/Microsoft/go-winio v0.4.11 // indirect
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/docker/distribution v2.7.0+incompatible // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/go-playground/form v3.1.3+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-test/deep v1.0.1
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang/groupcache v0.0.0-20181024230925-c65c006176ff // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/leebenson/conform v0.0.0-20180615210222-bc2e0311fd85
	github.com/leekchan/accounting v0.0.0-20180703100437-18a1925d6514 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/nicksnyder/go-i18n v1.10.0 // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v0.9.2 // indirect
	github.com/prometheus/client_model v0.0.0-20190115171406-56726106282f // indirect
	github.com/prometheus/common v0.0.0-20190107103113-2998b132700a // indirect
	github.com/prometheus/procfs v0.0.0-20190117184657-bf6a532e95b1 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/stretchr/testify v1.3.0
	go.opencensus.io v0.18.0
	golang.org/x/net v0.0.0-20190119204137-ed066c81e75e // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/sys v0.0.0-20190121090251-770c60269bf0 // indirect
	google.golang.org/api v0.1.0 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/go-playground/validator.v9 v9.25.0
)
