package main

import (
	"net/http"

	"flamingo.me/flamingo"
	"flamingo.me/flamingo/core/cache"
	"flamingo.me/flamingo/core/gotemplate"
	"flamingo.me/flamingo/core/locale"
	"flamingo.me/flamingo/framework/dingo"
	"flamingo.me/flamingo/framework/opencensus"
	"flamingo.me/flamingo/framework/router"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/app"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/db"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/metrics"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/migration"
	"gitlab.com/age-of-devops/flamingo-product-rating/src/products"
)

type application struct{}

func (a *application) Configure(injector *dingo.Injector) {
	injector.Bind((*cache.Backend)(nil)).ToInstance(cache.NewInMemoryCache())
	router.Bind(injector, a)
}

// Routes
func (a *application) Routes(registry *router.Registry) {
	registry.Route("/static/*n", "_static")
	registry.HandleGet("_static", router.HTTPAction(http.StripPrefix("/static/", http.FileServer(http.Dir("static")))))
}

func main() {
	flamingo.App(
		[]dingo.Module{
			new(locale.Module),
			new(gotemplate.Module),
			new(db.Module),
			new(migration.Module),
			new(products.Module),
			new(opencensus.Module),
			new(metrics.Module),
			new(app.Module),
			new(application),
		},
	)
}
